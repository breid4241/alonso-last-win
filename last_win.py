from flask import Flask, render_template

from datetime import date

import requests

import time

from pprint import pprint

app = Flask(__name__)

BASE_URL = "https://ergast.com/api/f1"


def get_races(session: requests.Session):
    
    driver_info = session.get(f"{BASE_URL}/drivers/alonso/results.json").json()

    offset = 0
    pages = int(driver_info["MRData"]['total']) % 50

    for page in range(pages + 1):
        next_page = session.get(f"{BASE_URL}/drivers/alonso/results.json", params={"limit": 50, "offset": offset + (page * 50)}).json()

        for season in next_page["MRData"]["RaceTable"]["Races"]:
            yield season

@app.route('/')
def days_since_last_win():

    session = requests.Session()
    print("Starting timer")
    tic = time.perf_counter()
    races_won = [race for race in get_races(session) if race['Results'][0]['position'] == '1']
    toc = time.perf_counter()

    print(f"Got results in {toc - tic:0.4f} seconds")

    latest_race_win = races_won[-1]

    # pprint(latest_race_win)

    todays_date = date.today()
    race_date_list = latest_race_win['date'].split("-")
    race_date = date(int(race_date_list[0]), int(race_date_list[1]), int(race_date_list[2]))

    days = (todays_date - race_date).days

    return render_template('index.html', days=days)